package ConnectionSQL;
/**
* @author anmijurane - *IntelliJ*
*/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLConnection {
    private static SQLConnection instans;
    private static Connection conn = null;


    private SQLConnection() {
        String url = "jdbc:mysql://localhost:3306/mensaje";
        String driver = "com.mysql.cj.jdbc.Driver";
        String usuario = "root";
        String password = "";

        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(url, usuario, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }//constructorEND

    public static SQLConnection getInstance(){
        if (instans == null){
            instans = new SQLConnection();
        }
        return instans;
    }

    public static Connection getconnection() {
        if (conn == null) {
            new SQLConnection();
        }
        return conn;
    }

}
