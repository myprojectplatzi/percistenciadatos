package index;

import ConnectionSQL.SQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 */
public class MensajesDAO {

    static Connection connec = SQLConnection.getconnection();
    static PreparedStatement pS = null;
    static ResultSet rS = null;


    public static void createMessageDB(Mensajes message) {
        try {
            String query = "INSERT INTO informacion(mensaje, autor_msj) VALUES (?, ?)";
            pS = connec.prepareStatement(query);
            pS.setString(1, message.getMensaje());
            pS.setString(2, message.getAutorMensaje());
            pS.executeUpdate();
            System.out.println("send message!");
        } catch (SQLException e) {
            System.out.println("SQL:" + e);
        }

    }

    public static void readMessageDB() {

        try {
            String query = "SELECT * FROM informacion";
            pS = connec.prepareStatement(query);
            rS = pS.executeQuery();

            while (rS.next()){
                System.out.println("----------MEENSAJE N° " + rS.getInt(1)+"----------");
                System.out.println("Autor: " +rS.getString(3));
                System.out.println("Fecha: " +rS.getTimestamp(4));
                System.out.println(rS.getString(2));
            }

        }catch (SQLException e){
            System.err.println("ERROR:" +e);
        }
    }

    public static void deleteMessagesDB(int idMessage) {

        try {
            String query = "DELETE FROM informacion where idMensaje = ?";
            pS = connec.prepareStatement(query);
            pS.setInt(1, idMessage);
            int deleteLines = pS.executeUpdate();
            if (deleteLines != 0) {
                System.out.println(deleteLines + "Lineas -> fueron eliminadas");
                System.out.println("Se elimino el ID: " +idMessage);
            }else{
                System.out.println("No se pudo eliminar el ID: " +idMessage);
            }
        }catch (SQLException e){
            System.err.println("ERROR"+e);
        }

    }

    public static void updateMessageDB(Mensajes message) {
        try{
            String query = "UPDATE informacion SET mensaje = ? " +
                    "WHERE autor_msj = ?";
            pS = connec.prepareStatement(query);
            pS.setString(1, message.getMensaje());
            pS.setString(2, message.getAutorMensaje());
            int updateMessage = pS.executeUpdate();

            if (updateMessage != 0){
                System.out.println("Se actualizo el mensaje del autor: \""
                        +message.getAutorMensaje()+"\" correctamente");
            }else {
                System.out.println("No hay autor con el nombre: " +message.getAutorMensaje());
            }

        }catch (SQLException e){
            System.out.println(e);
        }


    }


}
