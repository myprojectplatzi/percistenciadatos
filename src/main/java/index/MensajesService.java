package index;

import java.util.Scanner;

/**
 * Encargada de
 */

public class MensajesService {

    static Mensajes msnj = new Mensajes();
    static Scanner inpt = new Scanner(System.in);

    public static void createMessage() {
        System.out.println("Escribe tu nombre");
        msnj.setAutorMensaje(inpt.nextLine());
        System.out.println("Escribe tu mensaje: (¡280 caracteres!) ");
        msnj.setMensaje(inpt.nextLine());

        MensajesDAO.createMessageDB(msnj);
    }

    public static void listMessage() {
        MensajesDAO.readMessageDB();
    }

    public static void deleteMessages() {
        System.out.println("----- LISTA DE MENSAJES -----");
        MensajesDAO.readMessageDB();
        System.out.print("ID a Eliminar: ");
        int idDelete = inpt.nextInt();
        MensajesDAO.deleteMessagesDB(idDelete);

    }

    public static void updateMessage() {
        System.out.println("----- ACTUALIZAR MENSAJES -----");

        System.out.print("Ingresa el ID del Mensaje: ");
        int idMsnj = inpt.nextInt();

        System.out.print("Ingresa el autor del Mensaje: ");
        String autorMsj = new Scanner(System.in).nextLine();

        System.out.println("Ingresa el nuevo mesaje:");
        String messageNew = new Scanner(System.in).nextLine();

        if (!(idMsnj == 0 || autorMsj.equals("") || messageNew.equals(""))){
            msnj.setIdMensaje(idMsnj);
            msnj.setAutorMensaje(autorMsj);
            msnj.setMensaje(messageNew);
            MensajesDAO.updateMessageDB(msnj);
        }else{
            System.out.println("Llena todos los datos");
        }

    }
}
