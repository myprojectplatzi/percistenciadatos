package index;

import java.util.Scanner;

public class indexJava {

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        Scanner sc = new Scanner(System.in);

        int selected = 0;

        do {

            System.out.println("---------------");
            System.out.println("Applicación de Mensajes");
            System.out.println("1. Crear un mensaje");
            System.out.println("2. Listar un mensaje");
            System.out.println("3. Eliminar mensaje");
            System.out.println("4. Editar mensaje");
            System.out.println("0. Salir del programa");

            selected = sc.nextInt();

            switch (selected) {
                case 1: //createMessage
                    MensajesService.createMessage();
                    break;
                case 2: // list message
                    MensajesService.listMessage();
                    break;
                case 3: // delete message
                    MensajesService.deleteMessages();
                    break;
                case 4: // update message
                    MensajesService.updateMessage();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;
            }

        } while (selected != 5);
        sc.close();
    }

}
